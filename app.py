#! /usr/bin/env python3

import os
import time
from bottle import route, run, template, request, redirect, static_file

# Own modules
import my_data
import web_tools
import bottle_helper
import weather
import outfit
import upload_test_data
import data_transfer

attr = my_data.attr


@route('/static/<filename:re:.*.(js|css|jpg)>')
def static(filename):
    '''
    A route to serve the CSS and Javascript files ending in .css or .js
    It returns files in the local "Static" folder
    '''
    # The root="XXX" defines the local folder where files will be fetched
    return static_file(filename, root='./static/')


# A route for the weather icons
@route('/static/icons/<filename:re:.*.(svg|png)>')
def static(filename):
    # The root="XXX" defines the local folder where files will be fetched
    return static_file(filename, root='./static/icons/')


# Another route to serve the clothes pictures
@route('/Clothes/<category>/<filename:re:.*.(jpg|jpeg|png|gif)>')
def static(category, filename):
    return static_file(filename, root='./Clothes/' + category + '/')


# Main route
@route('/', method=['GET', 'POST'])
def mainpage():
    pd = bottle_helper.merge_post(request.POST)
    print("PD", pd)
    season = pd['season'] if 'season' in pd else ''
    # If no searson was given take spring
    # TODO: set current season as default
    season = season if season else 'spring'

    data = {}
    constraints = {'season': season}
    data['outfit'] = outfit.pull_outfit(constraints)
    data['weather_widget'] = weather.html_widget('bremen', online=False)
    return template('mainpage', **data)


@route('/entry', method=['GET', 'POST'])
def entry():
    data = {}
    pd = bottle_helper.merge_post(request.POST)
    upload = request.files.get('img_file')
    if upload:
        upload_folder = './Clothes/' + pd['category']
        if not os.path.isdir(upload_folder):
            os.makedirs(upload_folder)
        upload.save(upload_folder)
    if upload and pd:
        pd['image_file'] = upload_folder + os.sep + upload.filename
        my_data.insert(pd)
    return template('entry', data=data, attr=attr)


# Inventory page
@route('/inventory')
def inventory():
    data = my_data.dump()
    table = web_tools.html_table(data)
    return template('inventory', table=table)


# Load the test data
@route('/load_test_data')
def test_data():
    print('Loading test data')
    # Use ONLY ONE of the following methods and comment out the other
    # upload_test_data.insert_background()
    data_transfer.data_import()
    return template('redirect2inventory')


# Delete item in inventory and return to inventory
@route('/delete', method='GET')
def delete():
    getdata = dict(request.query.decode())
    # Delete image
    image_file = my_data.get_row(getdata['idx'])['image_file']
    if os.path.isfile(image_file):
        print("Deleting file:", image_file)
        os.remove(image_file)
    if 'idx' in getdata and getdata['idx']:
        my_data.delete(getdata['idx'])
    redirect('/inventory')


# The debug and reloader are convenient for development
# They reload the daemon every time you change something on the code
# The debug and reloader are convenient for development
run(host='127.0.0.1', port=8080, debug=True, reloader=True)
# run(host='0.0.0.0', port=8080, debug=True, reloader=True)
