#! /usr/bin/env python3

"""
This script/module handles the most common SQLite operations, including:
- insert: add a new entry to the database or update an existing one
    my_data.py insert '{"image_file": "SOME/PATH/Image.jpg", ...}'
- get: get a database entry with a given index
    my_data.py get 1
- delete: delete the entry with the given index
    my_data.py 1
- dump: dump the content of the whole database
    my_data.py dump

Additionally the "testdata" option tests the insert function by adding/
modifying two entries.

Usage as module:

 import my_data
 data = {"image_file": "SOME/PATH/Image.jpg", ...}
 my_data.insert(data)

"""

import sys
import sqlite3

# Name of the file containing the SQLite database
db_file = 'clothes.db'
# Name of the table
table_name = 'clothes'

# Define the layout of the SQL table
layout = {}
layout['id'] = 'INTEGER PRIMARY KEY'
layout['image_file'] = 'TEXT'  # Path to the image file
layout['category'] = 'TEXT'  # socks, shoes, pants, shirt, jacket, gloves, hat
layout['color'] = 'TEXT'  # Color or colors in the cloth, comma separated
layout['season'] = 'TEXT'  # summer, autum, winter, spring
layout['occasion'] = 'TEXT'  # work, casual, sport, home, costum
layout['waterproof'] = 'TEXT'  # work, casual, sport, home, costum
layout['other'] = 'TEXT'  # waterproof, ...

categories = ('other', 'hat', 'jacket', 'shirt', 'pants',  'socks', 'shoes')
colors = ('red', 'green', 'blue', 'yellow', 'grey', 'black', 'pink', 'grey')
image_sizes = dict.fromkeys(categories, '')

# Dictionary descriving some of the attributes
attr = {}
attr['category'] = {}
attr['category']['choices'] = categories
attr['category']['multiple'] = False
attr['category']['input_type'] = 'radio'
attr['category']['required'] = True

attr['color'] = {}
attr['color']['choices'] = colors
attr['color']['multiple'] = True
attr['color']['input_type'] = 'checkbox'

attr['season'] = {}
attr['season']['choices'] = ['summer', 'autum', 'winter', 'spring']
attr['season']['multiple'] = True
attr['season']['input_type'] = 'checkbox'

attr['occasion'] = {}
attr['occasion']['choices'] = ['work', 'casual', 'sport', 'home', 'costum']
attr['occasion']['multiple'] = True
attr['occasion']['input_type'] = 'checkbox'

attr['waterproof'] = {}
attr['waterproof']['choices'] = ['No', 'light', 'full', 'swimming']
attr['waterproof']['multiple'] = False
attr['waterproof']['input_type'] = 'radio'

attr['other'] = {}
attr['other']['mutiple'] = True
attr['other']['input_type'] = 'textarea'


def sql_run(sql: str, db_file: str = db_file) -> str:
    '''
    Runs an SQL command (provided as string) against an sqlite3 database
    stored on "db_file". We just need to provide the command, since the default
    database is defined above.
    '''
    conn = sqlite3.connect(db_file)
    c = conn.cursor()
    # print("SQL", sql)
    c.execute(sql)
    result = c.fetchall()
    conn.commit()
    conn.close()
    return result


def ensure_table(table: str = table_name) -> None:
    '''
    Ensures that a table exists
    '''
    kv_fields = ['"' + k + '" ' + v for k, v in layout.items()]
    fields_str = ', '.join(kv_fields)
    sql = 'CREATE TABLE IF NOT EXISTS "' + table + '" (' + fields_str + ');'
    sql_run(sql, db_file)

# Check on shell
# sqlite3 clothes.db ".schema $(sqlite3 clothes.db ".tables")"


def get_row(idx: int, table: str = table_name) -> dict:
    '''
    Get the row in the database with a given index
    If the index does not exist sql_run returns an empty list
    and get_row returns None
    '''
    sql = 'SELECT * FROM ' + table + ' WHERE "id"=' + str(idx) + ';'
    values_raw = sql_run(sql, db_file)
    if values_raw:
        values_tuple = values_raw[0]
        values = dict(zip(layout.keys(), values_tuple))
        return values


def insert(data: dict, table: str = table_name) -> None:
    '''
    Insert or update a dictionary of keys/values into the database,
    if the keys match the fields on the given table.
    If the 'id' is provided it will update the given fields on that row.
    '''
    fields = list(layout.keys())
    if 'id' in data:
        # print("We have an id")
        current_values = get_row(data['id'])
    else:
        # print("New entry")
        fields.remove('id')
        current_values = {}

    insert_dict = {}
    for field in fields:
        if field in data:
            insert_dict[field] = data[field]
        elif field in current_values:
            insert_dict[field] = current_values[field]
        else:
            insert_dict[field] = ''

    if not insert_dict:
        print('WARNING: invalid data cannot be saved into database.')
        print('  ', insert_dict)
        return

    keys_str = ', '.join(insert_dict.keys())
    vals_list = [str(v) for v in insert_dict.values()]
    vals_str = '", "'.join(vals_list)
    sql = 'INSERT OR REPLACE INTO ' + table + '(' + keys_str + ') VALUES("'
    sql += vals_str + '");'
    # print("SQL", sql)
    sql_run(sql)


def delete(idx: int, table: str = table_name) -> None:
    '''
    Delete the row with the given index
    '''
    sql = 'DELETE FROM ' + table + ' WHERE "id"==' + str(idx) + ';'
    sql_run(sql)


# Get list of table names in database
def get_tables() -> list:
    sql = 'SELECT name FROM sqlite_master WHERE type="table";'
    tables_db = sql_run(sql)
    tables_db = [t[0] for t in tables_db]
    return tables_db


# Get a dict with tables and their fields
def get_headers() -> dict:
    headers = {}
    for table in get_tables():
        sql = 'SELECT name FROM PRAGMA_TABLE_INFO("' + table + '");'
        out = sql_run(sql)
        headers[table] = [f[0] for f in out]
    return headers


# Return the content of all tables as JSON
def dump() -> list:
    headers = get_headers()
    data = {}
    for t, h in headers.items():
        sql = 'SELECT * FROM "' + t + '";'
        rows = sql_run(sql)
        tmp_rules = []
        for row in rows:
            tmp = dict(zip(headers[t], row))
            tmp_rules.append(tmp)
        if tmp_rules:
            data[t] = tmp_rules
    return data


def testdata():
    ensure_table(table_name)

    # Insert new value
    test_data = {}
    test_data['image_file'] = 'Clothes/pants/NO_PIC.jpg'
    test_data['colors'] = 'black, green'
    test_data['category'] = 'pants'
    test_data['season'] = 'spring'
    test_data['occasion'] = 'formal'
    insert(test_data)
    # Update existing value
    test_data['id'] = 1
    test_data['occasion'] = 'casual'
    insert(test_data)
    # Another entry
    del test_data['id']
    test_data['image_file'] = 'Clothes/pants/NO_PIC2.jpg'
    test_data['color'] = 'white'
    insert(test_data)


def interactive(argv):
    '''
    This function is called for interactive usage, from command line.
    '''
    import argparse
    import json

    usage_text = '''Examples:
    # Add a new entry:
      python3 my_data.py insert \
        '{"image_file": "SOME/PATH/Image.jpg", "colors": "red",
        "category": "pants", "season": "winter"}'
    # Show all entries in formatted JSON format:
    python3 my_data.py dump -j
    '''

    parser = argparse.ArgumentParser(
                           epilog=usage_text,
                           formatter_class=argparse.RawDescriptionHelpFormatter
                           )

    sp = parser.add_subparsers(help='Options', dest='option')
    sp_insert = sp.add_parser('insert',
                              help='Insert or update row in the database')
    sp_insert.add_argument('data', type=str,
                           help='Data to insert/update in JSON format')

    sp_get = sp.add_parser('get',
                           help='Get a given entry from the database')
    sp_get.add_argument('id', type=int,
                        help='Database index (id) of the entry to show')
    sp_get.add_argument('-j', '--json', action='store_true',
                        help='Display the data in (indented) JSON format')

    sp_delete = sp.add_parser('delete',
                              help='Delete a row from the database')
    sp_delete.add_argument('id', type=int,
                           help='Database index (id) of the entry to delete')

    sp_dump = sp.add_parser('dump',
                            help='Dump all the values from the database')
    sp_dump.add_argument('-j', '--json', action='store_true',
                         help='Display the data in (indented) JSON format')

    sp_test = sp.add_parser('testdata',
                            help='Add some test data')

    args = parser.parse_args(argv)
    # print("ARGS", args)

    if args.option == 'insert':
        data = json.loads(args.data)
        print('Inserting data')
        insert(data)
    elif args.option == 'get':
        print('Getting entry for index "' + str(args.id) + '":')
        data = get_row(args.id)
        if args.json:
            print(json.dumps(data, indent=4))
        else:
            print(data)

    elif args.option == 'delete':
        print('Deleting entry for index "' + str(args.id) +
              '" (if it exists):')
        delete(args.id)

    elif args.option == 'dump':
        data = dump()
        if args.json:
            print(json.dumps(data, indent=4))
        else:
            print(data)

    elif args.option == 'testdata':
        testdata()


# If called from command line, launch the interactive function
if __name__ == '__main__':
    interactive(sys.argv[1:])
