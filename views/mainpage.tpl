% include('header')
% include('navigation')

{{!weather_widget}}



% if outfit:

% include('menu_options')

<h2 style="text-align: center;">Here is the outfit:</h2>
<div id="closet">
  <table>
  % for category, item in outfit.items():
    % if item:
      <tr>
        <td>{{!category.capitalize()}}</td>
        % image_file = item['image_file'].lstrip('.') if 'image_file' in item else ''
        <td>
          <img style="width: 100%; max-width:100px;" src="{{!image_file}}" alt="Picture of item in category {{category}}">
        </td>
      </tr>
    % end
  % end
  </table>
</div>

% else:

<h3 style="color:red;"> WARNING: The closet is empty.</h3>
<a href="/load_test_data">Load some test data</a>

% end


% include('footer')
