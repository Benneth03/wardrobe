#! /usr/bin/env python3

'''
Tool to export and import the data internally, without HTTP requests
'''

import os
import json
import my_data

default_dir = 'data_backup'
categories = my_data.categories
image_extensions = ('.jpg', '.jpeg', '.png')


def write_item(filename: str, item: dict) -> None:
    with open(filename, 'w') as fh:
        print("II", type(item), item)
        json.dump(item, fh, indent=4)


def data_export(directory: str = default_dir) -> None:
    if not os.path.exists(directory):
        os.makedirs(directory)

    data = my_data.dump()
    if 'clothes' not in data or not data['clothes']:
        print('ERROR: not data to export')
        return

    clothes = data['clothes']

    for item in clothes:
        del item['id']
        image_file = item.pop('image_file')
        image_base = os.path.basename(image_file)
        image_dest = directory + os.sep + image_base
        json_filename = directory + os.sep + image_base + '.json'

        # Write the data into single JSON files
        write_item(json_filename, item)
        # Copy the image file
        with open(image_file, 'rb') as src, open(image_dest, 'wb') as dst:
            dst.write(src.read())


# Careful here! Do not call the function "import" because it will mess up the
# internal Python "import" function (used at the beginning of the files)
def data_import(directory: str = default_dir) -> None:

    for entry in os.listdir(directory):
        if entry.endswith(image_extensions):
            image_path = directory + os.sep + entry
            data_file = directory + os.sep + entry + '.json'
            if os.path.isfile(data_file):
                print('Importing data internally:', entry)
                with open(data_file) as fh:
                    data = json.load(fh)
                category = data['category']
                dest_dir = './Clothes' + os.sep + category
                img_dst = dest_dir + os.sep + entry
                if not os.path.exists(dest_dir):
                    os.makedirs(dest_dir)
                # Copy image file
                with open(image_path, 'rb') as src, open(img_dst, 'wb') as dst:
                    dst.write(src.read())

                data['image_file'] = img_dst
                my_data.insert(data)


if __name__ == '__main__':
    print('This will not do anything interactivelly, the functions must be ' +
          'called inplicitly')
    # data_export()
    # data_import()
